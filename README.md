# iplookup
`iplookup` is a small utility for resolving a MAC address to an IPv4 address and vice versa.

`iplookup` works by pinging the broadcast address of the subnet of a device. It then waits for a reply from the target host and extracts prints out the [ethernet frame](https://en.wikipedia.org/wiki/Ethernet_frame#Ethernet_II) and [IPv4](https://www.rfc-editor.org/rfc/rfc791) header of the reply. Thus giving you the MAC address of the host (in the source address field of the ethernet frame) and the IP address (in the source address field of the IPv4 header).

There are some caveats to this method:
- You will only get a reply if you are on the same subnet/LAN of the target.
- Some hosts are configured to not reply to broadcast pings
  - In these situations you can use the `-u` option to send pings to unicast addresses

## Installation
### ArchLinux
If you are on arch then you can install with `makepkg`:
```
$ makepkg -sri
```

### Manual
### Requirements
Requires [libpcap](https://github.com/the-tcpdump-group/libpcap)

On archlinux:
```
# pacman -S install libpcap
```

For manual installation
```
$ git submodule init && git submodule update
$ make
$ sudo make install
```
To install to a custom directory:
```
$ sudo make DESTDIR="/usr/sbin" install
```
To remove:
```
$ sudo make uninstall
```

## Usage

```
# iplookup [-u] [mac_address | ip_address]
```

Where the target is written as the typical 6 byte hexadecimal separated by colons (`11:22:33:AA:BB:CC`) or a IP address ('192.168.1.1.')

`-u` is used to insert the address provided into the destination address field in the header for that layer.
