#include<netinet/in.h>
#include<unistd.h>
#include<stdint.h>
#include<stdio.h>
#include<stdlib.h>
#include<signal.h>
#include<string.h>
#include<pcap.h>
#include<openinject.h>
#include<linux/if_ether.h>
#include<linux/ip.h>
#include<linux/ipv6.h>
#include<pthread.h>

#define NET_INDENT "\t"
#define TRANS_INDENT "\t\t"
#define BOLD_START "\033[1m"
#define BOLD_END "\033[0m"

#define IPV4_PROTO(hdr)         ((struct iphdr *)(hdr))->protocol
#define IPV4_ADDR_STR_T 16
#define IPV6_ADDR_STR_T 40

#define ETHERTYPE(frame)        ntohs((uint16_t *)(frame)[12])

uint8_t src_mac[ETH_ALEN], resolv_mac[ETH_ALEN];
OPENINJ_CTX *ctx;
pcap_t *handle;
int found = 0, frame_printed = 0, ipv4_printed = 0, ipv6_printed = 0;
int UNICAST = 0, LL_ADDR = 0, IP_ADDR = 0;

void usage()
{
    printf("Usage: iplookup [ipaddress | macaddress]\n");
    printf("\n  -d\tdevice - Interface to be used for scanning\n");
    printf("  -u\tunicast - Create packets with a unicast destination address (based on supplied address type)\n");
    printf("    \t          iplookup will allow you to specificy unicast MAC addresses but there\n");
    printf("    \t          are few hosts that repond to broadcast ICMP packets with unicast MAC addresses\n");
    printf("  -h\tShow this message\n");
}

int fatal(char *msg, int rc)
{
    fprintf(stderr, "FATAL: %s\n", msg);
    openinj_print_err(ctx);
    exit(rc);
}

int main(int argc, char *argv[])
{
    void *start_listening(void *);
    char *target_str, *device = NULL;
    openinj_frame frame = openinj_frame_init();

    if(argc < 2) {
        usage();
        return -1;
    }

    /* Parse CLI arguments */
    while(--argc){
        if((*++argv)[0] == '-') {
            switch((*argv)[1]) {
                case 'd':
                    device = *++argv;
                    --argc;
                    break;
                case 'u':
                    UNICAST = 1;
                    break;
                case 'h':
                    usage();
                    return -1;
                default:
                    usage();
                    return -1;
            }
        }
        else if(target_str == NULL)
            target_str = *argv;
    }

    uint32_t target_addr;
    /* Check if we're dealing with an IP address or a hw address */
    if(inet_pton(AF_INET, target_str, &target_addr) == 1)
        IP_ADDR = 1;
    else
        LL_ADDR = 1;

    ctx = openinj_ctx_init();

    int r;
    r = openinj_open_dev(ctx, device);
    if(r == -1)
        fatal("Failed to open IPv4 interface for scanning", r);
    else
        printf("Using device %s\n", ctx->device_name);

    /* Create the listening thread */
    pthread_t recv_id;
    pthread_create(&recv_id, NULL, start_listening, target_str);
    
    struct sockaddr_in *src_addr = (struct sockaddr_in *)ctx->ifa_addr;
    struct sockaddr_in *subnet_mask = (struct sockaddr_in *)ctx->ifa_netmask;

    /* Get devices MAC addr */
    r = openinj_get_device_mac(ctx->device_name, src_mac);
    if(r == -1)
        fatal("Couldn't get MAC addr of source device", r);
    
    uint8_t target_mac[ETH_ALEN];
    
    if(LL_ADDR && UNICAST) {
        r = openinj_macaddr(target_mac, target_str);
        if(r == -1)
            fatal("Couldn't convert target to MAC addr", r);
        fprintf(stderr, "WARNING: Not many hosts respond to broadcast pings with unicast MAC addresses\n");
    } else {
        r = openinj_macaddr(target_mac, "ff:ff:ff:ff:ff:ff");
        if(r == -1)
            fatal("Couldn't convert broadcast MAC addr", r);
    }

    printf("Looking for: %s\n", target_str);

    /* Construct the ethernet frame */
    r = openinj_build_ethernet(&frame,
            target_mac,
            src_mac,
            ETH_P_IP);
    
    if(r == -1)
        fatal("Couldn't construct ethernet frame", r);

    if(!(UNICAST && IP_ADDR))
        target_addr = src_addr->sin_addr.s_addr | ~subnet_mask->sin_addr.s_addr;

    r = openinj_build_ipv4(&frame,
        4,
        5,
        0,
        0,
        0,
        0,
        5,
        0x01,
        src_addr->sin_addr.s_addr,
        target_addr,
        NULL,
        0);

    if(r == -1)
        fatal("Couldn't construct IPv4 header", r);

    r = openinj_build_icmpv4(&frame,
        0x08,
        0,
        0x0201);
    
    if(r == -1)
        fatal("Couldn't construct ICMPv4 header", r);


    while(!found) {
        if(openinj_inject(ctx, &frame) == -1)
            fatal("Failed to inject", -1);
        /* Allow some time between each tranmission */
        usleep(1000000);
    }

    pthread_join(recv_id, NULL);
    openinj_ctx_free(ctx);
    return 0; 
}

/* Prints an ethernet frame in a human readable format
 * Returns the ethertype of the frame on success and -1
 * otherwise
 */
uint16_t print_ethernet_frame(void *hdr)
{
    printf("%s802.3 Ethernet Frame%s:\n", BOLD_START, BOLD_END);
    struct ethhdr *header = hdr;
    printf("  Source Address: ");
    for(int i = 0; i < ETH_ALEN; i++)
        printf("%02X%c", header->h_source[i], (i < ETH_ALEN - 1 ? ':' : '\n'));
    printf("  Destination Address: ");
    for(int i = 0; i < ETH_ALEN; i++)
        printf("%02X%c", header->h_dest[i], (i < ETH_ALEN - 1 ? ':' : '\n'));

    uint16_t ethtype = ntohs(header->h_proto);
    printf("  EtherType: 0x%04X\n", ethtype);
    return ethtype;
}

/* Prints an IPv4 header in a human readable format.
 * Returns IHL of header in bytes
 */
uint8_t print_ipv4_header(void *hdr)
{
    printf("%s%sIPv4 Header%s:\n", NET_INDENT, BOLD_START, BOLD_END);
    struct iphdr *header = hdr;
    printf("%s  Version: %d\n", NET_INDENT, header->version);
    printf("%s  IHL: %d words / %d bytes\n", NET_INDENT, header->ihl, header->ihl * 4);
    printf("%s  ToS: %d\n", NET_INDENT, header->tos);
    printf("%s  Total Length: %d bytes\n", NET_INDENT, ntohs(header->tot_len));
    printf("%s  Identification: 0x%04X\n", NET_INDENT, header->id);
    printf("%s  Fragment Offset: 0x%04X\n", NET_INDENT, ntohs(header->frag_off));
    printf("%s  TTL: %d\n", NET_INDENT, header->ttl);
    printf("%s  Protocol: %d\n", NET_INDENT, header->protocol);
    printf("%s  Header Checksum: 0x%04X\n", NET_INDENT, ntohs(header->check));
    char saddr[IPV4_ADDR_STR_T], daddr[IPV4_ADDR_STR_T];
    inet_ntop(AF_INET, &header->saddr, saddr, IPV4_ADDR_STR_T);
    inet_ntop(AF_INET, &header->daddr, daddr, IPV4_ADDR_STR_T);
    printf("%s  Source Address: %s\n", NET_INDENT, saddr);
    printf("%s  Destination address: %s\n", NET_INDENT, daddr);

    return header->ihl * 4;
}

/* Prints an IPv6 header in a human readable format.
 * Returns IP protocol number as specified in RFC 790.
 */
uint8_t print_ipv6_header(void *hdr)
{

    printf("%s%sIPv6 Header%s:\n", NET_INDENT, BOLD_START, BOLD_END);
    struct ipv6hdr *header = hdr;
    printf("%s  Verison: %d\n", NET_INDENT, header->version);
    printf("%s  Traffic Class: %d\n", NET_INDENT, header->priority);
    printf("%s  Flow Label: 0x%02X%02X%02X\n", NET_INDENT,
            header->flow_lbl[0], header->flow_lbl[1], header->flow_lbl[2]);
    printf("%s  Payload Length: %d\n", NET_INDENT, ntohs(header->payload_len));
    printf("%s  Next Header: %d\n", NET_INDENT, header->nexthdr);
    printf("%s  Hop Limit: %d\n", NET_INDENT, header->hop_limit);
    char saddr[IPV6_ADDR_STR_T], daddr[IPV6_ADDR_STR_T];
    inet_ntop(AF_INET6, &header->saddr, saddr, IPV6_ADDR_STR_T);
    inet_ntop(AF_INET6, &header->daddr, daddr, IPV6_ADDR_STR_T);
    printf("%s  Source Address: %s\n", NET_INDENT, saddr);
    printf("%s  Destination address: %s\n", NET_INDENT, daddr);

    return header->nexthdr;
}

void *start_listening(void *args)
{
    char *target = args;
    void listen_for_replys(u_char *args, const struct pcap_pkthdr *ptr,
            const u_char *capture);
    char pcap_errbuf[PCAP_ERRBUF_SIZE];
    struct bpf_program fp;

    handle = pcap_open_live(ctx->device_name, 2048, 1, 2000, pcap_errbuf);
    if(handle == NULL)
        fatal(pcap_errbuf, PCAP_ERROR);

    char filter[100], buffer[100];

    if(IP_ADDR)
        sprintf(filter, "src host %s and icmp", target);
    else
        sprintf(filter, "ether src %s and icmp", target);

    if(pcap_compile(handle, &fp, filter, 0, 0) == PCAP_ERROR)
        fatal("Couldn't compile filter", PCAP_ERROR);
    if(pcap_setfilter(handle, &fp) == PCAP_ERROR)
        fatal("Couldn't set filter", PCAP_ERROR);

    printf("Listening for reply...\n\n");
    pcap_loop(handle, -1, listen_for_replys, resolv_mac);

    return NULL;
}

void listen_for_replys(u_char *args, const struct pcap_pkthdr *ptr,
        const u_char *capture)
{

    if(!frame_printed) {
        print_ethernet_frame(capture);
        frame_printed = 1;
    }
    if(!ipv4_printed && ETHERTYPE(capture) == ETH_P_IP) {
        print_ipv4_header(capture + ETH_HLEN);
        ipv4_printed = 1;
    }
    else if(!ipv6_printed && ETHERTYPE(capture) == ETH_P_IPV6) {
        print_ipv6_header(capture + ETH_HLEN);
        ipv6_printed = 1;
    }

    if(frame_printed && ipv4_printed) { 
        found = 1;
        pcap_breakloop(handle);
    }
}
