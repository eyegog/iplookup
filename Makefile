CC = gcc
FLAGS = -Wall -g
OPENINJ_LIB = openinject/lib
DESTDIR = /usr/bin

arplookup: $(OPENINJ_LIB) 
	$(CC) $(FLAGS) iplookup.c -o iplookup -Iopeninject/include -lpcap -L$(OPENINJ_LIB) -lopeninject

$(OPENINJ_LIB):
	$(MAKE) -C openinject static

.PHONY: clean install
clean:
	$(MAKE) -C openinject clean
	rm -v iplookup

install:
	cp -v iplookup $(DESTDIR)

uninstall:
	rm -v $(DESTDIR)/iplookup
